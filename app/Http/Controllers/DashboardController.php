<?php

namespace App\Http\Controllers;

use App\Models\Redbook;
use App\Models\RevKal;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Inertia\Inertia;
use App\Exports\RedbookExport;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function goToDashboard()
    {
        return redirect(route('dashboard'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $request->query();

        if ($request->user()->default_area_type != null && !isset($filters[$request->user()->default_area_type])) {

            $filters[$request->user()->default_area_type] = $request->user()->default_area_value;

            if ($request->user()->default_area_type == 'cluster') {
                $filters['branch'] = Redbook::getTeritory($request->user()->default_area_type, $request->user()->default_area_value);
            }
        }

        $lists = [
            'date' => Redbook::getPeriodList(),
            'branch' => Redbook::getTeritoryList(),
            'cluster' => [],
            'kabupaten' => []
        ];

        $defaultDate = $filters['date'] ?? $lists['date'][0]['date_value'];
        $teritory = 'Kalimantan';
        $childType = 'Branch';

        if (isset($filters['branch']) && $filters['branch'] != 'All Branch') {

            $teritory = 'Branch ' . $filters['branch'];
            $lists['cluster'] = Redbook::getTeritoryList('cluster', 'branch', $filters['branch']);
            $childType = 'Cluster';

            if (isset($filters['cluster']) && $filters['cluster'] != 'All Cluster') {

                $teritory = 'Cluster ' . $filters['cluster'];
                $lists['kabupaten'] = Redbook::getTeritoryList('kota', 'cluster', $filters['cluster']);
                $childType = 'Kabupaten';

                if (isset($filters['kabupaten']) && $filters['kabupaten'] != 'All Kabupaten') {
                    $teritory = 'City ' . $filters['kabupaten'];
                }
            }
        }

        $redbook = Redbook::getSum($defaultDate, $filters);
        $child_mom = Redbook::getChildMom($defaultDate, $childType, $filters);

        $curDate =  Carbon::now()->subDays(2);

        $dailyRevDate = $curDate->format('Ym');
        $revkal = RevKal::getSum($defaultDate, $filters);

        $current_date = $curDate->format('F Y');
        $selected_year = substr($defaultDate, 0, 4);
        $selected_date = Carbon::createFromFormat('Ym', $defaultDate)->format('F Y');

        return Inertia::render('Dashboard', compact('teritory', 'redbook', 'revkal', 'selected_year', 'selected_date', 'current_date', 'lists', 'filters', 'child_mom'));
    }

    /**
     * Download Redbook Data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        $filters = $request->query();
        $date = $filters['date'];

        return Excel::download(new RedbookExport($filters), "redbook_kal_$date.xlsx");
    }
}
