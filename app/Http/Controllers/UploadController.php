<?php

namespace App\Http\Controllers;

use App\Models\RevKal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class UploadController extends Controller
{
    public function index()
    {
        return Inertia::render('Upload');
    }

    public function store(Request $request)
    {
        $request->validate([
            'wl' => 'required|mimes:csv,txt',
        ]);

        $dateYm = $request->input('dateYm');
        $dateY = substr($dateYm, 0, 4);
        $tableName = 'rev_kal_' . $dateY;
        $tableTemp = 'rev_kal_temp';

        $wlPath = $request->file('wl')->store('wl');
        $wlFile = addslashes(storage_path('app')) . '/' . $wlPath;

        $query = "CREATE TABLE IF NOT EXISTS $tableName LIKE $tableTemp

        LOAD DATA LOCAL INFILE '$wlFile'
        REPLACE INTO TABLE $tableName
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY '\"'
        LINES TERMINATED BY '\r\n'
        IGNORE 1 LINES;

        DELETE FROM $tableName WHERE DATE_FORMAT(mtd_dt, '%Y%m') = '$dateYm';";

        DB::connection()->getPdo()->exec($query);

        Storage::delete($wlPath);

        return back()->with(['success' => 'Sukses mengupload WL']);
    }
}
