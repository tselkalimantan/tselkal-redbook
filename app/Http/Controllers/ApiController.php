<?php

namespace App\Http\Controllers;

use App\Models\Redbook;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct(Request $request)
    {
        $req = $request->all();
        if ($req['pwnya'] != config('database.connections.mysql.password'))
            abort(401);
    }

    public function importRev(Request $request)
    {
        $request->validate([
            'uploadedFile' => 'required|mimes:csv,txt',
        ]);
        $req = $request->all();
        $filePath = $request->file('uploadedFile')->store('rev_kal');

        Redbook::import($req['period'], $filePath);
    }
}
