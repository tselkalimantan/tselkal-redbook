<?php

namespace App\Exports;

use App\Models\Redbook;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RedbookExport implements FromCollection, WithHeadings
{
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function headings(): array
    {
        return [
            'period',
            'branch',
            'cluster',
            'kabupaten',
            'cb',
            'rgb',
            'rgb_voice',
            'rgb_sms',
            'rgb_data',
            'rgb_digital',
            'du',
            'revenue',
            'rev_voice',
            'rev_sms',
            'rev_broadband',
            'rev_digital',
            'rech_mkios_b',
            'rech_mkios_bulk_b',
            'voucher_consume',
            'rech_modern_b',
            'rech_b_total',
            'rech_mkios_b_inner'
        ];
    }

    public function collection()
    {
        return Redbook::getAllByPeriod($this->filters);
    }
}
