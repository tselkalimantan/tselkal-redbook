<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class RevKal extends Model
{
    use HasFactory;

    public static function getSum($dateYm, $filters)
    {
        $dateY = substr($dateYm, 0, 4);

        $selectedTeritory = "'KALIMANTAN'";

        $branch = isset($filters['branch']) && $filters['branch'] != 'All Branch' ? $filters['branch'] : null;
        $cluster = isset($filters['cluster']) && $filters['cluster'] != 'All Cluster' ? $filters['cluster'] : null;
        $kabupaten = isset($filters['kabupaten']) && $filters['kabupaten'] != 'All Kabupaten' ? $filters['kabupaten'] : null;

        $whereBranch = null;
        $whereCluster = null;
        $whereKabupaten = null;

        if ($branch != null) {
            $whereBranch = " AND branch = '$branch'";
            $selectedTeritory = 'branch';
            if ($cluster != null) {
                $whereCluster = " AND cluster_sales = '$cluster'";
                $selectedTeritory = 'cluster_sales';
                if ($kabupaten != null) {
                    $whereKabupaten = " AND kabupaten = '$kabupaten'";
                    $selectedTeritory = 'kabupaten';
                }
            }
        }

        $q = DB::connection('hadoop')->select(DB::raw("SELECT
            mtd_dt,
            $selectedTeritory AS teritory,
            ROUND(SUM(rev), 0) AS all_rev,
            ROUND(SUM(IF(l1_name = 'Broadband', rev, NULL)), 0) AS broadband_rev,
            ROUND(SUM(IF(l1_name = 'Digital Services', rev, NULL)), 0) AS digital_rev
        FROM
            rev_kal_$dateY
            WHERE DATE_FORMAT(mtd_dt,'%Y%m') = '$dateYm'
            $whereBranch
            $whereCluster
            $whereKabupaten
        GROUP BY
            mtd_dt,
            $selectedTeritory
        UNION ALL
        SELECT
            NULL,
            $selectedTeritory AS teritory,
            FORMAT(SUM(rev), 0) AS all_rev,
            FORMAT(SUM(IF(l1_name = 'Broadband', rev, NULL)), 0) AS broadband_rev,
            FORMAT(SUM(IF(l1_name = 'Digital Services', rev, NULL)), 0) AS digital_rev
        FROM
            rev_kal_$dateY
            WHERE DATE_FORMAT(mtd_dt,'%Y%m') = '$dateYm'
            $whereBranch
            $whereCluster
            $whereKabupaten
        GROUP BY
            $selectedTeritory"));

        $arr = collect($q);
        $totalRev = $arr->last();
        $arr->pop();

        return [
            'teritory' => $selectedTeritory,
            'all_rev' => $arr->pluck('all_rev'),
            'broadband_rev' => $arr->pluck('broadband_rev'),
            'digital_rev' => $arr->pluck('digital_rev'),
            'total_rev' => $totalRev
        ];
    }
}
