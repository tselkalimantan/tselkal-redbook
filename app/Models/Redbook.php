<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Redbook extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function getPeriodList()
    {
        $periods = DB::table('hadoop.redbook_2021')->selectRaw('DATE_FORMAT(period, "%Y%m") as period')->groupByRaw('last_day(period)')->orderByRaw('last_day(period) DESC')->get();
        $date_arr = [];

        foreach ($periods as $dmm) {
            $formattedDate = Carbon::createFromFormat('Ym', $dmm->period)->format('F Y');
            $date_arr[] = [
                'date_text' => $formattedDate,
                'date_value' => $dmm->period
            ];
        }
        return $date_arr;
    }

    public static function getTeritoryList($type = 'branch', $parentType = null, $parentName = null)
    {
        return DB::table('hadoop.redbook_2021')->select($type)
            ->when($parentType, function ($query, $parentType) use ($parentName) {
                $query->where($parentType, $parentName);
            })->groupBy($type)->orderBy($type)->get();
    }

    public static function getTeritory($childType, $childName, $parentType = 'branch')
    {
        return self::select($parentType)->where($childType, $childName)->groupBy($parentType)->first()[$parentType];
    }

    public static function getAllByPeriod($filters)
    {
        $dateYm = $filters['date'];
        $branch = isset($filters['branch']) && $filters['branch'] != 'All Branch' ? $filters['branch'] : null;
        $cluster = isset($filters['cluster']) && $filters['cluster'] != 'All Cluster' ? $filters['cluster'] : null;
        $kabupaten = isset($filters['kabupaten']) && $filters['kabupaten'] != 'All Kabupaten' ? $filters['kabupaten'] : null;

        return self::where('period', $dateYm)
            ->when($branch, function ($query, $branch) {
                $query->where('branch', $branch);
            })->when($cluster, function ($query, $cluster) {
                $query->where('cluster', $cluster);
            })->when($kabupaten, function ($query, $kabupaten) {
                $query->where('kabupaten', $kabupaten);
            })->orderBy('period')->get();
    }

    public static function getSum($dateYm, $filters)
    {
        $dateYmNow = Carbon::now()->subDays(2)->format('Ym');
        $dateYmd = Carbon::createFromFormat('Ym', $dateYmNow)->subDays(2)->format('Y-m-d');

        if ($dateYmNow != $dateYm) {
            $dateYmd = Carbon::createFromFormat('Ym', $dateYm)->lastOfMonth()->format('Y-m-d');
        }

        $selectedTeritory = "'KALIMANTAN'";

        $branch = isset($filters['branch']) && $filters['branch'] != 'All Branch' ? $filters['branch'] : null;
        $cluster = isset($filters['cluster']) && $filters['cluster'] != 'All Cluster' ? $filters['cluster'] : null;
        $kabupaten = isset($filters['kabupaten']) && $filters['kabupaten'] != 'All Kabupaten' ? $filters['kabupaten'] : null;

        $whereBranch = null;
        $whereCluster = null;
        $whereKabupaten = null;

        if ($branch != null) {
            $whereBranch = " AND branch = '$branch'";
            $selectedTeritory = 'branch';
            if ($cluster != null) {
                $whereCluster = " AND cluster = '$cluster'";
                $selectedTeritory = 'cluster';
            }
            if ($kabupaten != null) {
                $whereKabupaten = " AND kabupaten = '$kabupaten'";
                $selectedTeritory = 'kabupaten';
            }
        }

        return DB::connection('hadoop')->select(DB::raw("SELECT
            FORMAT(m.cb, 0) AS cb,
            FORMAT(m.rgb, 0) AS rgb,
            m.rgb AS rgb_p,
            m.rgb_voice AS rgb_voice,
            m.rgb_sms AS rgb_sms,
            m.rgb_data AS rgb_data,
            m.rgb_digital AS rgb_digital,
            FORMAT(m.du, 0) AS du,
            FORMAT(m.revenue, 0) AS revenue,
            FORMAT(m.rech_mkios_b, 0) AS rech_mkios_b,
            FORMAT(m.rech_mkios_bulk_b, 0) AS rech_mkios_bulk_b,
            FORMAT(m.voucher_consume, 0) AS voucher_consume,
            FORMAT(m.rech_modern_b, 0) AS rech_modern_b,
            FORMAT(m.rech_b_total, 0) AS rech_b_total,
            FORMAT(m.rech_mkios_b_inner, 0) AS rech_mkios_b_inner,
            FORMAT((m.revenue - m1.revenue) / m1.revenue * 100, 2) AS revenue_mom,
            FORMAT((m.cb - m1.cb) / m1.cb * 100, 2) AS cb_mom,
            FORMAT((m.rgb - m1.rgb) / m1.rgb * 100, 2) AS rgb_mom,
            FORMAT((m.du - m1.du) / m1.du * 100, 2) AS du_mom,
            FORMAT(
                (m.voucher_consume - m1.voucher_consume) / m1.voucher_consume * 100,
                2
            ) AS voucher_consume_mom,
            FORMAT(
                (m.rech_b_total - m1.rech_b_total) / m1.rech_b_total * 100,
                2
            ) AS rech_b_total_mom,
            FORMAT(
                (m.rech_modern_b - m1.rech_modern_b) / m1.rech_modern_b * 100,
                2
            ) AS rech_modern_b_mom,
            FORMAT(
                (m.rech_mkios_b - m1.rech_mkios_b) / m1.rech_mkios_b * 100,
                2
            ) AS rech_mkios_b_mom,
            FORMAT(
                (m.rech_mkios_b_inner - m1.rech_mkios_b_inner) / m1.rech_mkios_b_inner * 100,
                2
            ) AS rech_mkios_b_inner_mom
        FROM
            (
                SELECT
                    $selectedTeritory AS teritori,
                    SUM(revenue) AS revenue,
                    SUM(cb) AS cb,
                    SUM(rgb) AS rgb,
                    SUM(du) AS du,
                    SUM(voucher_consume) AS voucher_consume,
                    SUM(rgb_voice) AS rgb_voice,
                    SUM(rgb_sms) AS rgb_sms,
                    SUM(rgb_data) AS rgb_data,
                    SUM(rgb_digital) AS rgb_digital,
                    SUM(rech_mkios_b) AS rech_mkios_b,
                    SUM(rech_mkios_bulk_b) AS rech_mkios_bulk_b,
                    SUM(rech_modern_b) AS rech_modern_b,
                    SUM(rech_b_total) AS rech_b_total,
                    SUM(rech_mkios_b_inner) AS rech_mkios_b_inner
                FROM
                    redbook_2021
                WHERE
                    period = '$dateYmd'
                    $whereBranch
                    $whereCluster
                    $whereKabupaten
                GROUP BY
                    teritori
            ) m
            LEFT JOIN (
                SELECT
                    $selectedTeritory AS teritori,
                    SUM(revenue) AS revenue,
                    SUM(cb) AS cb,
                    SUM(rgb) AS rgb,
                    SUM(du) AS du,
                    SUM(voucher_consume) AS voucher_consume,
                    SUM(rgb_voice) AS rgb_voice,
                    SUM(rgb_sms) AS rgb_sms,
                    SUM(rgb_data) AS rgb_data,
                    SUM(rgb_digital) AS rgb_digital,
                    SUM(rech_mkios_b) AS rech_mkios_b,
                    SUM(rech_mkios_bulk_b) AS rech_mkios_bulk_b,
                    SUM(rech_modern_b) AS rech_modern_b,
                    SUM(rech_b_total) AS rech_b_total,
                    SUM(rech_mkios_b_inner) AS rech_mkios_b_inner
                FROM
                    redbook_2021
                WHERE
                    period = DATE_SUB('$dateYmd', INTERVAL 1 MONTH)
                    $whereBranch
                    $whereCluster
                    $whereKabupaten
                GROUP BY
                    teritori
            ) m1 ON m.teritori = m1.teritori"))[0];
    }

    public static function getChildMom($dateYm, $childType, $filters)
    {
        $dateYmNow = Carbon::now()->subDays(2)->format('Ym');
        $dateYmd = Carbon::createFromFormat('Ym', $dateYmNow)->subDays(2)->format('Y-m-d');

        if ($dateYmNow != $dateYm) {
            $dateYmd = Carbon::createFromFormat('Ym', $dateYm)->lastOfMonth()->format('Y-m-d');
        }

        $branch = isset($filters['branch']) && $filters['branch'] != 'All Branch' ? $filters['branch'] : null;
        $cluster = isset($filters['cluster']) && $filters['cluster'] != 'All Cluster' ? $filters['cluster'] : null;

        $whereBranch = null;
        $whereCluster = null;

        if ($childType == 'Cluster') {
            $whereBranch = " AND branch = '$branch'";
        }
        if ($childType == 'Kabupaten') {
            $whereCluster = " AND cluster = '$cluster'";
        }

        $q = DB::connection('hadoop')->select(DB::raw("SELECT
            m.teritori,
            FORMAT(m.revenue, 0) AS revenue,
            FORMAT(m.rech_mkios_b, 0) AS rech_mkios_b,
            FORMAT(
                (m.revenue - m1.revenue) / m1.revenue * 100,
                2
            ) AS revenue_mom,
            FORMAT(
                (m.rech_mkios_b - m1.rech_mkios_b) / m1.rech_mkios_b * 100,
                2
            ) AS rech_mkios_b_mom
        FROM
            (
                SELECT
                    $childType AS teritori,
                    SUM(revenue) AS revenue,
                    SUM(rech_mkios_b) AS rech_mkios_b
                FROM
                    redbook_2021
                WHERE
                    period = '$dateYmd'
                    $whereBranch
                    $whereCluster
                GROUP BY
                    teritori
            ) m
            LEFT JOIN (
                SELECT
                    $childType AS teritori,
                    SUM(revenue) AS revenue,
                    SUM(rech_mkios_b) AS rech_mkios_b
                FROM
                    redbook_2021
                WHERE
                    period = DATE_SUB('$dateYmd', INTERVAL 1 MONTH)
                    $whereBranch
                    $whereCluster
                GROUP BY
                    teritori
            ) m1 ON m.teritori = m1.teritori"));

        $arr = collect($q);
        return ['child_type' => $childType, 'data' => $arr];
    }

    public static function import($dateYm, $filePath)
    {
        $dateY = substr($dateYm, 0, 4);
        $file = addslashes(storage_path('app')) . '/' . $filePath;

        $query = "CREATE TABLE IF NOT EXISTS rev_kal_$dateY LIKE rev_kal_temp;

        DELETE FROM
            rev_kal_$dateY
        WHERE
            DATE_FORMAT(mtd_dt, '%Y%m') = '$dateYm';

        LOAD DATA LOCAL INFILE '$file' INTO TABLE rev_kal_$dateY
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;";

        DB::connection()->getPdo()->exec($query);

        Storage::delete($filePath);
    }
}
