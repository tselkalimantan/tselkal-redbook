<?php

namespace Deployer;

require 'recipe/laravel.php';

set('ssh_multiplexing', false);
// Project name
set('application', 'redbook');

// Project repository
set('repository', 'https://tselkal.commander:klepon123@gitlab.com/tselkalimantan/tselkal-redbook.git');


// Set Timeout
set('default_timeout', null);

// [Optional] Allocate tty for git clone. Default value is false.
#set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('root@10.73.98.150')
    ->set('deploy_path', '/var/www/{{application}}');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
// before('deploy:symlink', 'artisan:migrate');
